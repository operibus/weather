package training.weather;

import org.junit.Test;
import training.weather.service.WeatherForecastService;
import java.io.IOException;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;

public class WeatherForecastServiceTest {

    @Test
    public void ok() throws IOException {
        WeatherForecastService weatherForecastService = new WeatherForecastService();
        String forecast = weatherForecastService.getWoeid("Madrid");
        assertNotEquals("", forecast);

    }

    @Test
    public void not_valid_city_name() throws IOException {
        WeatherForecastService weatherForecastService = new WeatherForecastService();
        String forecast = weatherForecastService.getWoeid("HHHDDDCCCKKKEJ");
        assertEquals("", forecast);
    }
}
