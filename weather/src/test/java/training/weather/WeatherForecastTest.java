package training.weather;

import java.io.IOException;
import java.util.Date;
import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;

public class WeatherForecastTest {

	@Test
	public void ok() throws IOException {
		WeatherForecast weatherForecast = new WeatherForecast();
		String forecast = weatherForecast.getCityWeather("Madrid", new Date());
		assertNotEquals("", forecast);
	}

	@Test
	public void without_city_name() throws IOException{
		WeatherForecast weatherForecast = new WeatherForecast();
		String forecast = weatherForecast.getCityWeather(null, new Date());
		assertEquals("", forecast);
	}

	@Test
	public void empty_city_name() throws IOException{
		WeatherForecast weatherForecast = new WeatherForecast();
		String forecast = weatherForecast.getCityWeather("", new Date());
		assertEquals("", forecast);
	}
}