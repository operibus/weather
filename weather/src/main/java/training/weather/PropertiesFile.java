package training.weather;

import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

public class PropertiesFile {
    public String printSingleProperties(String keyName) throws IOException {
        Properties prop = new Properties();
        ClassLoader loader = Thread.currentThread().getContextClassLoader();
        InputStream stream = loader.getResourceAsStream("application.properties");
        prop.load(stream);
        return prop.getProperty("weather.domain");
    }
}
