package training.weather.service;

import com.google.api.client.http.GenericUrl;
import com.google.api.client.http.HttpRequest;
import com.google.api.client.http.HttpRequestFactory;
import com.google.api.client.http.javanet.NetHttpTransport;
import org.json.JSONArray;
import org.json.JSONObject;
import training.weather.PropertiesFile;

import java.io.IOException;

public class WeatherForecastService {

    public PropertiesFile propertiesFile = new PropertiesFile();

    public String getWoeid(String city) throws IOException {
        String r = generateBaseRequest(propertiesFile.printSingleProperties("weather.domain") + "search/?query=" + city);
        JSONArray array = new JSONArray(r);

        return (array.length() > 0) ? array.getJSONObject(0).get("woeid").toString() : "";
    }

    public JSONArray getConsolidatedWeather(String woeid) throws IOException {
        String r = generateBaseRequest(propertiesFile.printSingleProperties("weather.domain") + woeid);
        JSONArray results = new JSONObject(r).getJSONArray("consolidated_weather");
        return results;
    }

    public String generateBaseRequest(String url) throws IOException {
        HttpRequestFactory rf = new NetHttpTransport().createRequestFactory();
        HttpRequest req = rf
                .buildGetRequest(new GenericUrl(url));

        return req.execute().parseAsString();
    }

}
