package training.weather;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.stream.StreamSupport;

import org.json.JSONArray;
import org.json.JSONObject;
import training.weather.service.WeatherForecastService;

public class WeatherForecast {

	public WeatherForecastService weatherForecastService = new WeatherForecastService();

	public String EMPTY_STRING = "";

	public Date dateTimeReceived;

	public String getCityWeather(String city, Date datetime) throws IOException {
		if (datetime == null) {
			this.setDateTimeReceived(new Date());
		}else{
			this.setDateTimeReceived(datetime);
		}
		if (datetime.before(new Date(new Date().getTime() + (1000 * 60 * 60 * 24 * 6))) && null != city && !EMPTY_STRING.equals(city)) {
			String woe = weatherForecastService.getWoeid(city);
			JSONArray results = weatherForecastService.getConsolidatedWeather(woe);
			return StreamSupport.stream(results.spliterator(), false)
					.map(val ->(JSONObject) val)
					.filter(val -> val.get("applicable_date").toString().equals(new SimpleDateFormat("yyyy-MM-dd").format(this.getDateTimeReceived())))
					.findFirst()
					.get().get("weather_state_name").toString();

		}
		return EMPTY_STRING;
	}

	public Date getDateTimeReceived() {
		return dateTimeReceived;
	}

	public void setDateTimeReceived(Date dateTimeReceived) {
		this.dateTimeReceived = dateTimeReceived;
	}
}
