# Introducción

**WeatherForecast** es un ejercicio similar a la [Weather kata](https://github.com/CodiumTeam/weather-kata) de [Codium Team](https://www.codium.team).

Se trata de una clase con un método público que devuelve la previsión del tiempo de una ciudad en una fecha concreta.

Para ello, esta clase utiliza una API externa (requiere conexión a internet): [www.metaweather.com](https://www.metaweather.com) 

Ejemplo:

```java
WeatherForecast weatherForecast = new WeatherForecast();
weatherForecast.getCityWeather("Madrid", new Date());
```


# Ejercicio

El ejercicio consiste en **refactorizar** el código para hacerlo más **mantenible**, ya que el código existente, aunque **funciona**, es muy difícil de entender. 
  
Para ello se pueden realizar múltiples modificaciones siempre que se mantenga el API público. Ejemplos de modificaciones: incluir tests automáticos, extraer métodos, renombrar variables, modificar tipos de datos, crear nuevas clases, añadir librerías, etc. 


# Documentación

La solución debería contener un fichero README donde se respondan estas preguntas:
- ¿Qué has empezado implementando y por qué?
  Lo primero ha sido crear una clase WeatherForecastService las partes de las llamadas que se realiza
  a la API meteorológica, de esta manera si necesitamos volver a obtener el identificador "woeid", tan solo
  habrá que realizar la llamada al método que se ha creado.
- ¿Qué problemas te has encontrado al implementar los tests y cómo los has solventado?
- ¿Qué componentes has creado y por qué? 
    La clase WeatherForecastService se ha creado para crear los métodos necesarios que tendrá WeatherForecast.
    La clase PropertiesFile para la obtención de las propiedades que se han añadido en el archivo properties.
    El archivo application.properties para almacenar los archivos de configuración que necesitemos. En este caso
    el dominio.
- Si has utilizado dependencias externas, ¿por qué has escogido esas dependencias?
    En este caso no he utilizado ninguna dependencia externa.
- ¿Has utilizado  streams, lambdas y optionals de Java 8? ¿Qué te parece la programación funcional?
    En este caso se han utilizado para sustituir el bucle for que ya existía. Me parece una manera más fácil de entender, más limpia
    y con menos código que utilizar, en términos generales. Por otro lado, una vez acostumbrado a la programación en objetos,
    el cambio de paradigma es más difícil de dominar.
- ¿Qué piensas del rendimiento de la aplicación? 
   En principio para una aplicación que no vaya a recibir muchas peticiones, el rendimiento sería correcto.
- ¿Qué harías para mejorar el rendimiento si esta aplicación fuera a recibir al menos 100 peticiones por segundo?
    Añadir el uso de lambda mejora el rendimiento del bucle. Por otro lado, el uso de Optional también mejoraría el rendimiento.
    Por otro lado, montarlo como si fuera una api-rest mejoraría también el rendimiento.
- ¿Cuánto tiempo has invertido para implementar la solución? Aproximadamente 4 horas
- ¿Crees que en un escenario real valdría la pena dedicar tiempo a realizar esta refactorización?
   Desde mi punto de vista, todo lo que sean mejoras siempre son bienvenidas, por el tiempo que se puede dedicas
   a optimizar esta aplicación, creo que merece la pena.


# A tener en cuenta

- Se valorará positivamente el uso de TDD, se revisarán los commits para comprobar su uso.
- Se valorará positivamente la aplicación de los principios SOLID y "código limpio".
- La aplicación ya tiene un API público: un método público que devuelve el tiempo en un String. No es necesario crear nuevas interfaces: REST, HTML, GraphQL, ni nada por el estilo.


# Entrega

La solución se puede subir a un repositorio de código público como [github](https://github.com/). 

Otra opción sería comprimir la solución (incluyendo el directorio .git) en un fichero .zip/.tar.gz y enviarlo por email.
